"""
This script scrapes song data from Joynathu.com, arguably Malawi's #1 Urban Music website for latest renditions.
My due acknowlegements to Mr. Joy Nathu, the website's proprietor and MBC Radio 2, the proprietor's employers and sponsors.

Developed by Ramsey Njema Junior.
"""
import requests
from bs4 import BeautifulSoup
from pandas import DataFrame
from pathlib import Path

CUR_DIR = Path.cwd()

URL = "http://joynathu.com/"
USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36"

headers = {'User-Agent' : USER_AGENT}

with requests.Session() as session:
    try:
        page = session.get(URL, headers=headers)
        if page.status_code != 200:
            raise Exception
    except Exception as e:
        print("Encountered ERROR!", e)
    else:
        soup = BeautifulSoup(markup=page.text, features='html.parser')
        
        #Returns a Listset of list items each of which is a song element 
        songList = soup.find_all('li', attrs={'class' : 'song-unit'})
        
        parsedList = []
        for song in songList:
            title = song.find('span', attrs={'class' : 'song-title'}).text
            authors = song.find('span', attrs={'class' : 'song-author'}).text
            songUrl = "{}{}".format(URL, (song.find('div', attrs={'class' : 'singleSong-jplayer'})).get('data-mp3'))
            artworkUrl = "{}{}".format(URL, (song.find('img').get('src')))
            el = {'Title' : title, 'Artist(s)' : authors, 'Artwork' : artworkUrl, 'Link' : songUrl}
            parsedList.append(el)

        dataframe = DataFrame(parsedList)
        output = CUR_DIR / 'premieres.csv'
        with open(output, 'w') as f:
            dataframe.to_csv(f,index=False)
