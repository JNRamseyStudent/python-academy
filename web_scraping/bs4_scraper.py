from bs4 import BeautifulSoup
import requests as req

# Get ahold of website to scrape
response = req.get('http://books.toscrape.com/catalogue/category/books/travel_2/index.html')

if response.status_code != 200:
    print("OOH, Something must've gone wrong {status}".format(status=response.status_code))
else:
    page_soup = BeautifulSoup(response.content, 'html.parser')
    with open('travel_books.html', 'wb') as html_saved:
        html_saved.write(page_soup.prettify().encode('utf-8'))

content  = page_soup.find(class_='col-sm-8 col-md-9')

book_articles = content.find_all(class_='product_pod')
books = []

for article in book_articles:
    article_title = article.find('h3').text
    print(article_title)

print('\nWe found {pages} pages'.format(pages=len(book_articles)))