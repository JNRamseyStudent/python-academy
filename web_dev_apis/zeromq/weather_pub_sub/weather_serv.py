"""
DUMMY Weather Update Server
Binds PUB socket to tcp://127.0.0.1:5556
Publishes random weather updates
"""

import zmq

import random
from datetime import datetime

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://127.0.0.1:5556")

while True:
    zipcode = random.randrange(1,100000)
    temperature = random.randrange(14,32) + 3
    relhumidity = random.randrange(1,50) + 10
    print("[INFO] Generated %s*C for zipcode %s with humidity %s" % (temperature, zipcode, relhumidity))
    socket.send(("%s %s %s" % (zipcode, temperature, relhumidity)).encode("utf-8"))
