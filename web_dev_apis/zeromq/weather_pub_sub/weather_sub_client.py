"""
Weather Update Client
Connects SUB socket to tcp://localhost:5556
Collects weather updates and finds average in zipcode
"""

import sys
import zmq

#Socket to talk to server
context = zmq.Context()
socket = context.socket(zmq.SUB)

print("Collecting updates from weather server" + "."*5)
socket.connect("tcp://localhost:5556")

#Subscribes to zipcode, default is NYC, 10001
zip_filter = sys.argv[1] if len(sys.argv) > 1 else "10001"
socket.setsockopt(zmq.SUBSCRIBE, zip_filter.encode('utf-8'))

#Process 5 updates
total_temp = 0
for update_ in range(5):
    msg = socket.recv().decode('utf-8')
    
    zipcode, temp, relhum = msg.split()
    
    total_temp += int(temp)

print(f"Average temperature for zipcode `{zip_filter}` was {total_temp/update_}*C")
