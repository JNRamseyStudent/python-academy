"""
DUMMY Hello World Server
Binds REP socket to tcp://127.0.0.1:5555
Replies to a client hello message request with a `world` message"""

import zmq
import time as tm

context  = zmq.Context()

socket = context.socket(zmq.REP)
socket.bind("tcp://127.0.0.1:5555")

while True:
    client_msg = socket.recv().decode("utf-8")
    print("Received %s" % (client_msg))
    
    tm.sleep(1)

    socket.send("World".encode("utf-8"))    
