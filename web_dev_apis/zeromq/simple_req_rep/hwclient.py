"""
DUMMY Hello World Client
COnnects REQ socket to tcp://localhost:5555
Sends "Hello" to server, expects "Worls" back
"""

import zmq

context = zmq.Context()

print("Connecting to Hello world server")

socket = context.socket(zmq.REQ)

socket.connect("tcp://localhost:5555")

#Do 10 requests, waiting each time for a response
for req_ in range(10):
    print(f"Sending request {req_ + 1} ...")
    socket.send("Hello".encode('utf-8'))
    msg = socket.recv().decode("utf-8")
    print(f"Received reply {req_ + 1} with message: [{msg}]")




