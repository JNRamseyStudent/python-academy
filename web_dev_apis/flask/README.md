#Web-based MAC Discovery

## Author
Ramsey Ith Njema Jr.

## Project Purpose
Discover MAC addresses of all clients accessing the website

## Motivation
Automate the process of MAC address knowledge for non-technical personnel.
