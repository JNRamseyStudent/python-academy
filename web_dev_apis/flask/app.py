from flask import Flask, request, jsonify, render_template, redirect
#from api.arp_com import arp_resolve

app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello from Ith Jr."

@app.route('/ip')
def ip_identify():
    if request.environ.get('HTTP_X_FORWARDED_FOR') is None:
        ip_addr = request.environ['REMOTE_ADDR']
    else:
        ip_addr = request.environ['HTTP_X_FORWARDED_FOR']
    
    #0return arp_resolve(ip_addr)
    return ip_addr
@app.route('/login', methods=['GET', 'POST'])
def login():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')