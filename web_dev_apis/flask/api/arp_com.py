import sys
import asyncio

def arp_resolve(ip):
    """ Does ARP resolution thus obtains MAC address of the given ip
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Achieves this by initially attempting to ping the given IPv4 address; updating local ARP table
    in the process"""
